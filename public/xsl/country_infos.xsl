<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:param name="name"/>

<xsl:template match="/">

    <xsl:for-each select="//country[name=$name]">
        <div class="">
            <h2><xsl:value-of select="name"/>
                <span class="badge badge-warning mr-1" id="cc">
                    <xsl:value-of select="name/@cc"/>
                </span>
            </h2>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-4">
                <img class="img-fluid" src="{flag/@src}"/>
            </div>
            <div class="col-sm-4">
                <p>
                    <b> Capital city : </b> <xsl:value-of select="capital"/><br/>
                    <br/>
                    <b>Population : </b> <span id="population"><xsl:value-of select="population"/> </span> inhabitants<br/>
                    <br/>
                    <b>Area : </b> <span id="area"> <xsl:value-of select="area"/> </span> km<sup>2</sup><br/>
                </p>
            </div>
        </div>
        <hr></hr>
        <div class="row">
            <h2> Where is it ?</h2>
            <iframe
                width="100%"
                height="400"
                frameborder="0"
                marginheight="0"
                marginwidth="0"
                src="https://maps.google.com/maps?q={name}&amp;h1=en&amp;output=embed">
            </iframe>
        </div>
    </xsl:for-each>

</xsl:template>

</xsl:stylesheet>
