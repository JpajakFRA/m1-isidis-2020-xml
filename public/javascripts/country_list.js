let search = '';
let continent = '';

async function display() {
    const xml = await get('/xml/countries.xml');
    const xsl = await get('/xsl/country_list.xsl');

    const proc = new XSLTProcessor();
    proc.importStylesheet(xsl);
    proc.setParameter(null,'search_country',search);
    proc.setParameter(null,'search_continent',continent);

    const result = proc.transformToFragment(xml, document);

    document.querySelector('#frag_country_list').innerHTML = '';
    document.querySelector('#frag_country_list').appendChild(result);
}


display();

document.querySelector('#search_country').onkeyup = function(){
    search=this.value;
    display();
};

document.querySelector('#search_continent').onclick = function(){
        continent = this.value;
        display();
};

