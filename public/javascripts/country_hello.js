let cc='';

async function getCountryCode(){
    const xml = await get('../xml/countries.xml','txt');

    const country_name = new URL(location).searchParams.get('country');

    const parser = new DOMParser();
    let xmldoc = parser.parseFromString(xml,'text/xml');

    let xpath = '//country[name="'+country_name+'"]/name/@cc';
    let xPathResult = xmldoc.evaluate(xpath,xmldoc,null,XPathResult.STRING_TYPE,null);
    cc = xPathResult.stringValue;
}

async function sayHello() {

    await getCountryCode();

    let apiRequest = 'https://fourtonfish.com/hellosalut/?cc='+cc;
    const apiResponse = await get(apiRequest,'txt');

    const obj = JSON.parse(apiResponse);
    let hello = obj.hello;

    document.getElementById('say_hello').innerHTML = hello;
}

sayHello();