async function display() {

    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_infos.xsl');


    const name = new URL(location).searchParams.get('country');


    const xsltProcessor = new XSLTProcessor();
    xsltProcessor.setParameter(null,'name',name);
    xsltProcessor.importStylesheet(xsl);
    const result = xsltProcessor.transformToFragment(xml, document);

    document.querySelector('#frag_country_infos').appendChild(result);
}

//Fonction pour le formatage des nombres au format En : ',' pour chaque ^3
async function numberFormat(){
    await display();
    document.getElementById('population').innerText = new Intl.NumberFormat('en-EN').format(document.getElementById('population').innerText);
    document.getElementById('area').innerText = new Intl.NumberFormat('en-EN').format(document.getElementById('area').innerText);
}

numberFormat();